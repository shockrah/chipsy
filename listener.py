import time
import os, sys
import socket
import subprocess
import deepspeech
import numpy as np
import pyaudio
import requests

from src import wavSplit
from src import wavTranscriber

BUFFER_SIZE = 5248122880
TIME_SLICE_SIZE = 5.0
ACTIVATION_PHRASE = 'chip'

def to_nlp(segment: str):
    '''
    Takes the segment collected and yeets it as hard as possible to a local
    natual language processor that is hopefully way faster and thus lets this 
    do its thing normally
    '''
    # doing this over udp to not wait on shit
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(segment, 'ascii'), ('127.0.0.1', 1248))
    sock.close()


def collect_segment(model: deepspeech.Model, t_start: float, listenproc) -> str:
    stream = model[0].createStream()
    while (time.time() - t_start) <= TIME_SLICE_SIZE:
        data = listenproc.stdout.read(BUFFER_SIZE)
        stream.feedAudioContent(np.frombuffer(data, np.int16))
                        
    return stream.intermediateDecode()


if __name__ ==  '__main__':
    output_graph, scorer = wavTranscriber.resolve_models(os.getcwd())
    model = wavTranscriber.load_model(output_graph, scorer)

    context = model[0].createStream()
    sproc = subprocess.Popen('rec -q -V0 -e signed-integer -L -c 1 -b 16 -r 16k -t raw - gain -2'.split(),
                             stdout=subprocess.PIPE,
                             bufsize=0)
    print('=====================')
    print('ready')

    try:
        keyword = ACTIVATION_PHRASE
        cur = ''
        last = ''
        t_spent = time.time()
        while True:
            now = time.time()
            data = sproc.stdout.read(BUFFER_SIZE)
            context.feedAudioContent(np.frombuffer(data, np.int16))
            cur = context.intermediateDecode()
            if keyword in cur or (now - t_spent >= TIME_SLICE_SIZE):
                context.freeStream()

                if keyword in cur:
                    segment = collect_segment(model, now, sproc)
                    to_nlp(segment)

                # rebuild the context after each cmd-listen call
                context = model[0].createStream()
                t_spent = now
            else:
                if cur != last:
                    last = cur


    except KeyboardInterrupt:
        print('ded x.x')

