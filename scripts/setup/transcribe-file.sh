#!/bin/bash

source bin/activate

for f in audio/*.wav; do
	bin/deepspeech --model deepspeech-0.9.2-models.pbmm --scorer deepspeech-0.9.2-models.scorer --audio $f
done

