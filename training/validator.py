# Custom validator for local labels

# In our case we don't really want anything specific
def validate_label(label):
    # make sure we dont bother with capitals as they're agita +
    # we don't need them for the language parser
    return label.lower()



